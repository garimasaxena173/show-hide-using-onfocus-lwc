import { LightningElement, track } from 'lwc';

export default class SearchBar extends LightningElement {

    @track isRendered;
    @track searchKey;
    // Temporary data 
    @track searchResultList = [{
        id: 1,
        dataItem: 'result 1',
    }, {
        id: 2,
        dataItem: 'result 2',
    }, 
    ];

    handleFocus(){
        this.isRendered = true; 
    }

    handleInputChange(event){
        this.searchKey = event.target.value;
    }
}